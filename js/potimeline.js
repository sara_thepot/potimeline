/* 
 * Potimeline
 *
 * Version: 1.1
 *  Author: Sara Potyscki (ThePot)
 * Website: http://www.thepot.site/portfolio/potimeline
 * License: Released under the MIT license [http://www.thepot.site/portfolio/potimeline#license]
 *
 */


(function (d,w) {
    'use strict';
		
	w.Potimeline = function(divContainer, json, options = null){
		if (options == null) {
			options = {"nothing": true};
		}
		return this.init(divContainer, json, options);
	};
	
	Potimeline.prototype = {
		_nodeContainer : null,
		_nodeBar : null,
		_nodeBarMover : null,
		_nodePanelList : null,
		
		
		_jsonRanges : null,
		_jsonElements : null,
		
		_barCenter : 0,
		_barStartX : 0,		
		_arrYears: [],	
		_barElementWidth: 0,
		_numBarElements: 0,
		
		_dragging: false,		
		_initialMouseX: null,
		_currentMouseX: null,
		_currentMouseDirection: null,
		_moveAtX : 0,
		
		_callback : function(){},
		
		options : {
			init : function(){},
			selected : function(){},
			defaultImage: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASIAAACuCAMAAAClZfCTAAAAIVBMVEX19fXd3d3z8/Pq6urb29vi4uLx8fHt7e3n5+fk5OTf39/UY198AAACNElEQVR4nO3a4Y6rIBCGYRUE9P4veO0KijB022xSTOd9zj/Xk9AvMgjOMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgDualrGy+t6juhNXB7Sxofe4bmQVIxqt6z2w25jkhMbR9B7ZbcjzbMNMS3wroqX3yG7DtCJae4+sm+lqEFb8FNFU3qtCaAXyilVDSN7+J6Jx7j3+D2gWnkYkFzrK05OI1sV457wJ2avk9T9b1RHZOduTTcdd19qjOqK13GwEIro+QsJ7tPudbi5f8p3iWiSfe8zS0/bh4fYgRdQ6GRJOAJRGdG7oJx+CyaoSEe2Ot0GfZtZRmer3TJ0RpUVryfJIU6/avqmMKJ14XAqPjRlVR0kqI4qlp3xe4uVyVdMYUfzNVdVJ14koleZ6fd+nWnmurTGiPQlXH5HEha7ITmNETrz6sN9fFCOFEdl9yZeOIve/FGVcY0T7VSLK8BT9Sa5Fwqe0GAa1KO1h6xUtvnWzoqUkqs1Y7HmYiuw0RhR/c7UZW57e/tWqN6C4YTVWjKJ861YZUTouCpeM4gkJO/38McpWNXu0hFRbN50Rnb/arKPd/i3ulZu/l7Aby9uInMu+nAm9WUojanWjSQ1+WiOSMxL7+9RGJPXEyDfqjajqjnXSp1jlEW2z7fzG6OdWo5buiB4NRsGYsDQ61YnoNRoiEg7y36GiFbvZiE5Cp7KZ+g29hw4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIbhB3oYDvixwDtCAAAAAElFTkSuQmCC",
			htmlListLi: null,
			urlPrefix: null
			
		},
		
		
		init : function(divContainer, json, options){
			var _this = this;
			
			for(var i in options){
				this.options[i] = options[i];
			}
			this._nodeContainer = divContainer;
			this._jsonRanges = json.ranges;
			this._jsonElements = json.elements;
			
			for(var i in this._jsonRanges){
				if (_this._arrYears.indexOf(this._jsonRanges[i]) === -1){
					_this._arrYears.push(this._jsonRanges[i]);
				}
			}
			this._arrYears.sort();
			this._numBarElements = this._jsonRanges.length;
			
			this._generateBar();
			this._generatePanel();
		
			window.onresize = function(){
				_this._onWindowResize();
			};
			
			//this.options.init();
		},
		_generateBar : function(){
			var _this = this;
			
			this._nodeBar = document.createElement("div");
			this._nodeBar.addClass("potimeline-bar");
			this._nodeContainer.appendChild(this._nodeBar);
			
			this._nodeBarMover = document.createElement("ul");
			this._nodeBarMover.addClass("potimeline-mover");
			this._nodeBar.appendChild(this._nodeBarMover);
						
			for(var i in this._arrYears){
				var nodeBarElement = document.createElement("li");
				var nodeBarElementSpan = document.createElement("span");
				nodeBarElementSpan.appendChild(document.createTextNode(this._arrYears[i]));
				nodeBarElement.appendChild(nodeBarElementSpan);
				
				nodeBarElement.setAttribute("data-year",this._arrYears[i]);
				
				_this._nodeBarMover.appendChild(nodeBarElement);
				nodeBarElement.addEventListener("click", _this._clickBarElement.bind(null, _this));				
			}
			getNode(".potimeline-bar li:first-child").addClass("selected");
			
			this._barElementWidth = getNode(".potimeline-bar li:first-child").offsetWidth;
			this._nodeBarMover.style.width = (this._numBarElements * this._barElementWidth) + "px";
			this._barCenter = this._nodeBar.clientWidth / 2;
			this._barStartX = this._nodeBar.offsetLeft + (this._barElementWidth / 2);
			
			
			
			this._nodeBar.onmousedown = function(e){
				e.stopPropagation(); 
				_this._dragging = true;
				_this._nodeBarMover.addClass("noanimation");
				_this._initialMouseX = e.clientX;
				_this._currentMouseX = e.clientX;
			};
			
			
			window.onmousemove = function(e){
				e.stopPropagation(); 
				if (_this._dragging) {
					if (e.clientX > _this._currentMouseX) {
						_this._currentMouseDirection = "R";
					}else{
						_this._currentMouseDirection = "L";
					}
					_this._currentMouseX = e.clientX;
					
					var move = _this._currentMouseX - _this._initialMouseX;
					
					if ((_this._moveAtX + move) < 0 && (_this._moveAtX + move) > -((_this._numBarElements-1) * _this._barElementWidth)) {
						_this._nodeBarMover.style.webkitTransform = "translateX("+ (_this._moveAtX + move) +"px)";
					}
					
				}
			};
			
			window.onmouseup = function(e){
				if (_this._dragging) {
					e.stopPropagation();
					
					var move = _this._currentMouseX - _this._initialMouseX;
					
					var slots = Math.round(move / _this._barElementWidth);
					
					_this._nodeBarMover.removeClass("noanimation");
					_this._dragging = false;
					
					if (move != 0) {
						_this._moveAtX = _this._moveAtX + (slots * _this._barElementWidth);
						
						if (_this._moveAtX > 0) {
							_this._moveAtX = 0;
						}else if (_this._moveAtX < -((_this._numBarElements-1) * _this._barElementWidth)) {
							_this._moveAtX = -((_this._numBarElements-1) * _this._barElementWidth);
						}
						
						_this._nodeBarMover.style.webkitTransform = "translateX("+ (_this._moveAtX) +"px)";
						
						
						
						var elementIndex = (Math.floor(_this._moveAtX / _this._barElementWidth) * -1) + 1;
						console.log(elementIndex);
						
						getNode(".potimeline-bar .selected").removeClass("selected");
						
						var nodeBarElement = getNode(".potimeline-bar li:nth-child("+elementIndex+")");
						nodeBarElement.addClass("selected");
						_this._loadPanelContent(nodeBarElement.getAttribute("data-year"));
						
					}
				}
			};
			
			
		},	
		_clickBarElement : function(_this, event){
			//var nodeBarElement;
			//if (event.target.tagName == "LI") {
			//	nodeBarElement = event.target;			
			//}else{
			//	nodeBarElement = event.target.parentElement;
			//}
			
			if (event.target.tagName == "LI") {
				var nodeBarElement = event.target;
				if (!nodeBarElement.hasClass("selected")) {
					
					getNode(".potimeline-bar .selected").removeClass("selected");
					nodeBarElement.addClass("selected");
					var slot = Array.prototype.indexOf.call(nodeBarElement.parentNode.children, nodeBarElement);
					_this._moveAtX = -(slot * _this._barElementWidth);
					_this._nodeBarMover.style.webkitTransform = "translateX("+_this._moveAtX+"px)";
					_this._loadPanelContent(nodeBarElement.getAttribute("data-year"));
				}			
			}
		},	
		_generatePanel : function(){
			var _this = this;
			
			var nodePanel = document.createElement("div");
			nodePanel.className = "potimeline-panel";
			this._nodeContainer.appendChild(nodePanel);
			
			this._nodePanelList = document.createElement("ul");
			this._nodePanelList.className = "potimeline-list";
			nodePanel.appendChild(this._nodePanelList);
			
			
			_this._loadPanelContent(this._arrYears[0]);
		},
		_loadPanelContent : function(arg){
			var _this = this;
			var rangeMin = arg;
			var rangeMax = 999999999;
			
			if (this._arrYears.indexOf(parseInt(rangeMin)) < (this._arrYears.length - 1)) {
				rangeMax = this._arrYears[this._arrYears.indexOf(parseInt(rangeMin)) + 1];
			}
			this._nodePanelList.removeClass("potimeline-loaded");
			this._nodePanelList.innerHTML = "";
			for(var i in this._jsonElements){
				var date = new Date(this._jsonElements[i].date);	
				if (date.getFullYear() >= rangeMin && date.getFullYear() < rangeMax){
					
					var url;
					if(this.options.urlPrefix){
						url = this.options.urlPrefix + this._jsonElements[i].url;
					}else{
						url = this._jsonElements[i].url;
					}
					
					if (_this.options.htmlListLi) {
						
						var attributes = "";
						for (var j in this._jsonElements[i].attributes) {
							attributes += j + "=\"" + this._jsonElements[i].attributes[j] + "\" ";
						}
						var html = _this.options.htmlListLi.replace("{{url}}", url).replace("{{thumb}}",this._jsonElements[i].thumb).replace("{{attributes}}",attributes);
						this._nodePanelList.innerHTML += html;
						
					}else{
						var nodePanelElement = document.createElement("li");
						this._nodePanelList.appendChild(nodePanelElement);
						
						var nodePanelElementLink = document.createElement("a");
						nodePanelElementLink.setAttribute("href", url);
						nodePanelElement.appendChild(nodePanelElementLink);
						
						var nodePanelElementImg = document.createElement("img");
						nodePanelElementImg.setAttribute("src", this._jsonElements[i].thumb);
						nodePanelElementLink.appendChild(nodePanelElementImg);
					}
					
					
					
					
					
				}
			}
			
			
			
			var images = this._nodePanelList.querySelectorAll("img");
			var imageCount = images.length;
			var imagesLoaded = 0;
			
			var _this = this;
			var arrIntervals = {};
			
			for(var i=0; i<imageCount; i++){
				
				if(images[i].complete){
					arrIntervals[i] = setInterval(function(images, i){
						if (typeof images[i].naturalWidth !== "undefined" && images[i].naturalWidth > 0) {
							imagesLoaded++;
							if(imagesLoaded == imageCount){
								_this.options.selected(); //callback
								_this._nodePanelList.addClass("potimeline-loaded");
							}
							clearInterval(arrIntervals[i]);
						}
					},100,images,i);
					
				}else{
				
					images[i].addEventListener('load',function(){
						imagesLoaded++;
						if(imagesLoaded == imageCount){
							_this.options.selected(); //callback
							_this._nodePanelList.addClass("potimeline-loaded");
						}	
					});
				
					images[i].addEventListener('error',function(){
						event.target.onerror = "";
						event.target.src = _this.options.defaultImage;
					});
				}
			}
			
			
			
		},
		_onWindowResize : function(){
			var _this = this;			
			this._barCenter = this._nodeBar.clientWidth / 2;
			this._barStartX = this._nodeBar.offsetLeft + (this._nodeBar.clientWidth / 2);
		}
		
	}		
		
	
})(document,window);

